const path = require('path');
// const TerserPlugin = require('terser-webpack-plugin');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        'hello-world': './src/pages/hello-world.js',
        'kiwi': './src/pages/kiwi.js'
    },
    output: {
        // filename: 'js/bundle.[contenthash].js',
        filename: 'js/[name].bundle.js',
        path: path.resolve(__dirname, './dist'),
        publicPath: ''
    },
    mode: 'development',
    devServer: {
        contentBase: path.resolve(__dirname, './dist'),
        index: 'index.html',
        port: 3001
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg|jpeg)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    /*MiniCssExtractPlugin.loader*/ 'style-loader', 'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    /*MiniCssExtractPlugin.loader*/ 'style-loader', 'css-loader', 'sass-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader:'babel-loader',
                    options: {
                        presets: [ '@babel/env' ],
                        plugins: [ 'transform-class-properties' ]
                    }
                }
            },
            {
                test: /\.hbs$/,
                use: [
                    'handlebars-loader'
                ]
            },
            {
                test: /\.ejs$/,
                use: [
                    'ejs-loader'
                ]
            }
        ]
    },
    plugins: [
        // new TerserPlugin(),
        // new MiniCssExtractPlugin({
        //     filename: 'css/style.[contenthash].css'
        // }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [
                '**/*',
                path.join(process.cwd(), 'build/**/*')
            ]
        }),
        new HtmlWebpackPlugin({
            title: 'Hello EJS',
            template: 'src/templates/hello-world-template.ejs',
            filename: 'hello-world.html',
            chunks: ['hello-world'],
            description: 'More description HELLO...'
        }),
        new HtmlWebpackPlugin({
            title: 'Kiwi EJS',
            template: 'src/templates/kiwi-template.ejs',
            filename: 'kiwi.html',
            chunks: ['kiwi'],
            description: 'More description KIWI...'
        })
    ]
};