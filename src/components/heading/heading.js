import './heading.scss';

class Heading {
    render() {
        const h1 = document.createElement('h1');
        h1.setAttribute("id", "h1-id");
        const body = document.querySelector('body');
        h1.innerHTML = 'Webpack is awesome. YES';
        body.appendChild(h1);
    }
}

export default Heading;